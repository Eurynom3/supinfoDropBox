package main

import (
	"net/http"
	"gitlab.com/Eurynom3/supinfoDropBox/api"
	"gitlab.com/Eurynom3/supinfoDropBox/api/handlers"
	"github.com/spf13/viper"
	"os"
	"os/signal"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
	"context"
	"github.com/rs/cors"
)

func main() {

	// Init shutdown signal
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	// Init conf
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/supfile/")
	viper.SetConfigName("config")

	viper.ReadInConfig()

	// Init context
	ctx := api.InitCtx()

	// Init routes
	r := handlers.Get(ctx)

	// Setup http server
	port := strconv.Itoa(viper.GetInt("port"))

	// CORS is pain
	c := cors.Options{
		AllowedOrigins: viper.GetStringSlice("origins-allowed"),
		AllowedMethods: []string{"GET", "DELETE", "POST", "PATCH", "PUT", "OPTIONS"},
		AllowedHeaders: []string{"Accept", "content-type", "Content-Length", "Same-Origin","Authorization", "application/json", "Accept-Language", "origin","volume","userid","access-control-allow-same-origin", "access-control-allow-origin", "Access-Control-Allow-Origin", "access-control-allow-headers"},
		Debug: true,
	}

	h := &http.Server{Addr: ":" + port, Handler: cors.New(c).Handler(r)}

	// Launching API
	go func() {
		logrus.Printf("Listening on http://0.0.0.0:%s\n", port)

		if err := h.ListenAndServe(); err != nil {
			logrus.Fatal(err)
		}
	}()


	// Catching graceful shutdowns
	<-stop

	logrus.Println("Shutting down the server...")

	// Shutdowns, finishing last requests
	ctxShtDwn, _ := context.WithTimeout(context.Background(), 30*time.Minute)
	h.Shutdown(ctxShtDwn)

	logrus.Println("Server gracefully stopped")

}
