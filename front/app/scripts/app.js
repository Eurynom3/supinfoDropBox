'use strict';

/**
 * @ngdoc overview
 * @name firstAppApp
 * @description
 * # firstAppApp
 *
 * Main module of the application.
 */
angular
    .module('firstAppApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngFileSaver',
        'com.2fdevs.videogular',
        'ngTouch'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'app/views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/about', {
                templateUrl: 'app/views/about.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/profile', {
                templateUrl: 'app/views/profile.html',
                controller: 'ProfileCtrl',
                controllerAs: 'profile'
            })
            .when('/filesPage', {
                templateUrl: 'app/views/filesPage.html',
                controller: 'FilesCtrl',
                controllerAs: 'files'
            })
            .otherwise({
                redirectTo: '/about'
            });
    });
