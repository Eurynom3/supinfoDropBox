'use strict';

/**
 * @ngdoc function
 * @name firstAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the firstAppApp
 */
var APIURL = 'http://localhost:9000';
angular.module('firstAppApp')
    .controller('Parent', function ($scope, $location) {
        $scope.$on('$routeChangeStart', function ($event, next, current) {
            $scope.islog = localStorage.getItem('token');
            console.log($scope.islog);
        });

        $scope.disco = function () {
            localStorage.clear();
            $location.path('/main');
        };
    })


    .controller('MainCtrl', function ($scope, $http, $location) {
        $scope.form = {
            email: '',
            password: ''
        };

        $scope.formIn = {
            email: '',
            pwd: '',
            pwd2: ''
        };

        $scope.gurl = APIURL + '/google/connect';

        $scope.signGoogle = function () {
            console.log("here");

            var profile = googleUser.getBasicProfile();
            $scope.gform = {
                email: profile.getEmail(),
                password: profile.getId()
            };

            $http.post($scope.gurl, $scope.gform)
                .then(function (response) {
                    localStorage.setItem('userID', response.data.userID);
                    localStorage.setItem('token', response.data.token);
                    $location.path('/profile');
                })
                .catch(function (reason) {
                    console.log(reason);
                });
        };

        $scope.submitCo = function () {
            $scope.url = APIURL + '/connect';
            $http.post($scope.url, $scope.form)
                .then(function (response) {
                    console.log(response);
                    console.log(response.data);
                    localStorage.setItem('userID', response.data.userID);
                    localStorage.setItem('token', response.data.token);
                    $location.path('/profile');
                })
                .catch(function (reason) {
                    console.log(reason);
                });
        };

        $scope.submitLo = function () {
            if ($scope.formIn.pwd != $scope.formIn.pwd2) {
                console.log('bite');
            }
            else {
                $scope.url = APIURL + '/register';
                var toSend = {
                    email: $scope.formIn.email,
                    password: $scope.formIn.pwd
                };
                $http.post($scope.url, toSend)
                    .then(function (response) {
                        $location.path('/main');
                    })
                    .catch(function (reason) {
                        console.log(reason);
                    });

            }
        };

    })

    .controller('FilesCtrl', function ($scope, $http, FileSaver, Blob, $sce) {
        $scope.urlGetArch = APIURL + '/folder/content?path=';
        $scope.arch = [];
        $scope.error = '';
        $scope.reussite = '';
        console.log(localStorage.getItem('token'));

        $http.get($scope.urlGetArch + '/', {
            headers: {
                'Authorization': localStorage.getItem('token'),
                'userID': localStorage.getItem('userID')
            }
        })
            .then(function (response) {
                $scope.arch = response.data;
                console.log(response);
            })
            .catch(function (reason) {
                $scope.error = 'Problème dans la récupération des données';
            });


        $scope.getArch = function (pathToSend) {
            var url = $scope.urlGetArch + pathToSend;
            $http.get(url, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'userID': localStorage.getItem('userID')
                }
            })
                .then(function (response) {
                    $scope.arch = response.data;
                })
                .catch(function (reason) {
                    $scope.error = 'Problème dans la récupération des données';
                });
        };
        $scope.folderForCreation = '';

        $scope.form = {
            file: ''
        };

        $scope.upload = function (current) {
            var url = APIURL + '/file';
            var f = document.getElementById('file').files[0];
            if (f == undefined) {
                return;
            }
            var fd = new FormData();
            console.log(f);
            fd.append('contentFile', f);
            $http.post(url, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-type': undefined,
                    'Authorization': localStorage.getItem('token'),
                    'userID': localStorage.getItem('userID'),
                    'Path': current
                }
            })
                .then(function () {
                    $scope.reussite = 'Upload réussie !';
                    $scope.error = '';
                })
                .catch(function () {
                    $scope.error = 'Problème dans lors de l\'upload du fichier';
                    $scope.reussite = '';
                });

        };

        $scope.download = function (file) {
            var url = APIURL + '/file/' + file.id;
            $http.post(url, {}, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'Content-type': file.extension
                },
                responseType: 'arraybuffer'

            })
                .then(function (response) {

                    var contentType = response.headers("Content-Type");

                    var data = new Blob([response.data], {type: contentType});
                    FileSaver.saveAs(data, file.name);

                })
                .catch(function (reason) {
                    $scope.error = 'Problème dans la récupération du fichier';
                });
        };

        $scope.downloadFolder = function (id) {
            console.log($scope.arch);
            var url = APIURL + '/folder/' + id;
            $http.post(url, {}, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'Content-type': 'application/zip'
                },
                responseType: 'arraybuffer'

            })
                .then(function (response) {

                    var data = new Blob([response.data], {type: 'application/zip'});
                    FileSaver.saveAs(data, "archive.zip");

                })
                .catch(function (reason) {
                    $scope.error = 'Problème dans la récupération du fichier';
                });
        };

        $scope.deleteFile = function (id) {
            console.log(id);
            var url = APIURL + '/file/' + id;
            $http.delete(url, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'userID': localStorage.getItem('userID')
                }
            })
                .then(function (value) {
                    $scope.reussite = 'Delete réussi !';
                    $scope.error = '';
                })
                .catch(function (reason) {
                    $scope.error = 'Le fichier n\'a pas été supprimé';
                    $scope.reussite = '';
                });

        };

        $scope.shared = '';
        $scope.haveShared = false;
        $scope.shareFile = function (id) {
            console.log(id);
            var url = APIURL + '/file/share/' + id;
            $http.get(url, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'userID': localStorage.getItem('userID')
                }
            })
                .then(function (value) {
                    $scope.reussite = 'Partage réussi !';
                    $scope.shared = APIURL + value.data;
                    $scope.haveShared = true;

                    $scope.error = '';
                })
                .catch(function (reason) {
                    $scope.error = 'Le fichier n\'a pas été partagé';
                    $scope.reussite = '';
                });

        };

        $scope.addFolder = function () {
            console.log($scope.arch.current + $scope.folderForCreation);
            var url = APIURL + '/folder';
            var path = {
                path: $scope.arch.current + $scope.folderForCreation
            };

            $http.post(url, path, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'userID': localStorage.getItem('userID')
                }
            })
                .then(function (value) {
                    $scope.reussite = 'Creation réussie !';
                    $scope.error = '';
                })
                .catch(function (reason) {
                    $scope.error = 'Le dossier n\' a pas été créé';
                    $scope.reussite = '';
                });
        };

        $scope.deleteFolder = function (id) {
            console.log(id);
            var url = APIURL + '/folder/' + id;
            $http.delete(url, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'userID': localStorage.getItem('userID')
                }
            })
                .then(function (value) {
                    $scope.reussite = 'Delete réussi !';
                    $scope.error = '';
                })
                .catch(function (reason) {
                    $scope.error = 'Le dossier n\' a pas été supprimé';
                    $scope.reussite = '';
                });
        };

        $scope.visualisation = false;
        $scope.src = '';
        $scope.videoConf = {};


        function _arrayBufferToBase64(buffer) {
            var binary = '';
            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;
            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        }

        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        };
        $scope.isImage = function (type) {
            return (type == 'image/jpeg' || type == 'image/png')
        };
        $scope.isVideo = function (type) {
            return (type == 'video/mp4' || type == 'image/ogg' || type == 'image/webm')
        };

        $scope.visualize = function (type, id) {
            $scope.visualisation = true;
            $scope.type = type;

            if ($scope.isImage(type)) {
                $http.get(APIURL + '/file/' + id, {
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    },
                    responseType: "arraybuffer"
                }).then(function (data) {
                    $scope.src = 'data:image/jpeg;base64,' + _arrayBufferToBase64(data.data);
                });
            }

            if (type == "application/pdf") {
                $http.get(APIURL + '/file/' + id, {
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    },
                    responseType: "arraybuffer"
                }).then(function (data) {
                    var file = new Blob([data.data], {type: 'application/pdf'});
                    $scope.src = URL.createObjectURL(file);
                });
            }
            console.log(type, $scope.isVideo(type));

            if ($scope.isVideo(type)) {

                $scope.videoConf = {
                    src: [
                        {
                            src: $scope.trustSrc(APIURL + '/stream/' + id), type: type
                        }
                    ],

                    tracks: [],
                    theme: {
                        url: "https://unpkg.com/videogular@2.1.2/dist/themes/default/videogular.css"
                    }
                };
                console.log("here", $scope.srcV);
            }
        };


    })

    .controller('ProfileCtrl', function ($scope, $http) {
        // $http.get(url, {
        //     headers: {
        //         'Authorization': localStorage.getItem('token'),
        //         'userID': localStorage.getItem('userID')
        //     }
        // })
        //     .then(function (value) {
        //         $scope.user = value;
        //     })
        //     .catch(function (reason) {
        //
        //     });
        $scope.reussite = '';
        $scope.error = '';


        $scope.formPro = {
            current: '',
            pwd: '',
            pwd2: ''
        };

        var urlSwap = APIURL + '/password';
        $scope.changePwd = function () {
            if ($scope.formPro.pwd != $scope.formPro.pwd2) {
                $scope.error = 'Les deux mot de passes ne sont pas identiques !';
                $scope.reussite = '';
                $scope.formPro.pwd = '';
                $scope.formPro.pwd2 = '';
                return
            }
            console.log(localStorage.getItem('token'));
            $http.post(urlSwap, $scope.formPro, {
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    'userID': localStorage.getItem('userID')
                }
            })
                .then(function (value) {
                    $scope.reussite = 'Changement réussi !';
                    $scope.error = '';
                    $scope.formPro.current = '';
                    $scope.formPro.pwd = '';
                    $scope.formPro.pwd2 = '';
                })
                .catch(function (reason) {
                    $scope.error = 'Le mot de passe n\'a pas pu être changé';
                    $scope.reussite = '';
                });
        };

    })

;

