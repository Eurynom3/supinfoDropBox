CREATE TABLE disks(
  id BIGSERIAL PRIMARY KEY,
  size BIGINT NOT NULL,
  path TEXT
);

CREATE EXTENSION pgcrypto;
CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  login VARCHAR(255),
  password TEXT,
  have_facebook BOOL,
  have_google BOOL,
  allocated_bytes BIGINT,
  disk_id BIGINT REFERENCES disks(id),
  prime BOOL
);
CREATE INDEX ON users (login);

CREATE TYPE status AS ENUM ('synced', 'todelete', 'torename');

CREATE TABLE files (
  id BIGSERIAL PRIMARY KEY,
  folder_id BIGINT REFERENCES folders(id) ON DELETE CASCADE,
  name VARCHAR(255),
  size BIGINT,
  extension VARCHAR(255),
  shared BOOL DEFAULT false,
  user_id BIGINT REFERENCES users(id),
  creation_date TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
  last_modification TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
  storage_name VARCHAR(255),
  current_status status DEFAULT 'synced'
);
ALTER TABLE files ADD CONSTRAINT file_per_folder_unique UNIQUE(name, folder_id);
CREATE INDEX ON files(folder_id, name, user_id);

CREATE EXTENSION ltree;
CREATE TABLE folders (
  id BIGSERIAL PRIMARY KEY,
  path LTREE,
  user_access BIGINT[],
  user_id BIGINT REFERENCES users(id),
  previous_folder BIGINT REFERENCES folders(id),
  storage_name VARCHAR(255),
  current_status status DEFAULT 'synced'
);
ALTER TABLE folders ADD CONSTRAINT folder_unique UNIQUE (path, user_id);
CREATE INDEX ON folders USING GIST(path);
CREATE INDEX ON folders USING GIN (user_access);


CREATE FUNCTION propagate_status() RETURNS trigger AS
  $BODY$
  BEGIN
    UPDATE files
      SET current_status = NEW.current_status
    WHERE folder_id = NEW.ID;
    RETURN NEW;
  END
  $BODY$
  LANGUAGE plpgsql;

CREATE TRIGGER flag_folder_delete_after_update
  AFTER UPDATE OF current_status ON folders
  FOR EACH ROW EXECUTE PROCEDURE propagate_status();

CREATE TABLE direct_links(
  id BIGSERIAL PRIMARY KEY,
  link uuid,
  file_id BIGINT REFERENCES files(id) ON DELETE CASCADE
);
CREATE INDEX ON direct_links (link);