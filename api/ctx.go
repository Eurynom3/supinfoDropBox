package api

import (
	"database/sql"

	"github.com/spf13/viper"
	"gitlab.com/Eurynom3/supinfoDropBox/api/database/connect"
)

type SUPFileCtx struct {
	DB          *sql.DB
	StoragePath string
}

// InitCtx setups database connection and server storage path
func InitCtx() SUPFileCtx {
	// Init worker pool

	return SUPFileCtx{
		DB:          connect.Database("supfile"),
		StoragePath: viper.GetString("storage-path"),
	}
}
