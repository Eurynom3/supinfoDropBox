package handlers

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/mholt/archiver"
	"github.com/rs/zerolog"
	"gitlab.com/Eurynom3/supinfoDropBox/api"
	"gitlab.com/Eurynom3/supinfoDropBox/api/database"
	"gitlab.com/Eurynom3/supinfoDropBox/api/storage"
)

const (
	headerError   = "could not read header"
	cannotUse     = "file can not be parsed"
	IllegalFormat = "illegal format"
)

var (
	logger       zerolog.Logger
	DecodeLogger zerolog.Logger
)

func init() {
	logger = zerolog.New(os.Stderr).With().Timestamp().Str("type", "[HANDLER]").Logger()
	DecodeLogger = zerolog.New(os.Stderr).With().Timestamp().Str("type", "[DECODE]").Logger()

}

type Sizer interface {
	Size() int64
}

// AddFile is the handler to add a file to the server
func AddFile(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Iterating on every sent files
		for _, headers := range r.MultipartForm.File {

			for _, header := range headers {

				body, err := header.Open()
				if err != nil {
					http.Error(w, headerError, http.StatusBadRequest)
					return
				}

				var size int64

				// Getting uploaded file size
				switch t := body.(type) {
				case *os.File:
					var fi os.FileInfo
					fi, err = t.Stat()
					size = fi.Size()

				default:
					size = body.(Sizer).Size()
				}

				if err != nil {
					http.Error(w, cannotUse, http.StatusNotAcceptable)
					return
				}
				if size > 30*storage.GB {
					http.Error(w, cannotUse, http.StatusNotAcceptable)
					return
				}

				fBytes, err := ioutil.ReadAll(body)
				fType := http.DetectContentType(fBytes)
				if storage.HasAnIllegalFormats(fType) {
					http.Error(w, IllegalFormat, http.StatusNotAcceptable)
					return
				}

				// Adding file to database
				var (
					insertedID int
					path       string
					uid        int
					vol        string
					ok         bool
				)
				if uid, vol, ok = extractAllFromToken(r); !ok {
					http.Error(w, illegalToken, http.StatusUnauthorized)
					return
				}

				if path, insertedID, err = database.AddFile(&ctx, r.Header.Get("Path"), vol, header.Filename, fType, size, uid); err != nil {
					if strings.Contains(err.Error(), "duplicate key") {
						http.Error(w, alreadyExists, http.StatusConflict)
						return
					}
					if strings.Contains(err.Error(), "not enough space") {
						http.Error(w, insufficentStorage, http.StatusInsufficientStorage)
						return
					}
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				// Adding file to server
				if err = storage.AddFile(ctx, path, fBytes); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					if err = database.DeleteFile(&ctx, insertedID, uid); err != nil {
						logger.Warn().Int("ID", insertedID).Msg("[DATA] Could not delete it in database after we failed to add it in storage")
					}
					return
				}

				w.Write([]byte("Success"))
				return
			}
		}

		logger.Warn().Str("uuid", r.Form.Get("uuid")).Interface("request", *r).Msg("No files in request")
		http.Error(w, "There is no file to add", http.StatusBadRequest)

	})
}

type FolderContent struct {
	Files     []database.File   `json:"files"`
	Folders   []database.Folder `json:"folders"`
	Current   string            `json:"current"`
	CurrentID int               `json:"currentId"`
}

// GetFile is the handler for downloading a file
func GetFolderContent(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var (
			folderContent FolderContent
			err           error
			uid           int
			ok            bool
		)
		if uid, ok = extractUserID(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		folderContent.Files, err = database.GetFiles(&ctx, r.Form.Get("path"), uid)
		if err != nil {
			http.Error(w, "Could not get your files :(", http.StatusInternalServerError)
			return
		}

		folderContent.CurrentID, folderContent.Folders, err = database.GetFolders(&ctx, r.Form.Get("path"), uid)
		if err != nil {
			http.Error(w, "Could not get your folders :(", http.StatusInternalServerError)
			return
		}

		folderContent.Current = r.Form.Get("path")

		json.NewEncoder(w).Encode(&folderContent)

	})
}

// GetFile is the handler for downloading a file
func GetFile(ctx api.SUPFileCtx, stream bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)

		fid, err := strconv.Atoi(vars["id"])
		if err != nil {
			http.Error(w, "Wrong file ID format", http.StatusBadRequest)
			return
		}

		var (
			path string
			uid  int
			ok   bool
			vol  string
		)
		if !stream {
			if uid, vol, ok = extractAllFromToken(r); !ok {
				http.Error(w, illegalToken, http.StatusUnauthorized)
				return
			}
			if path, err = database.GetFilePath(&ctx, vol, fid, uid); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			if path, err = database.GetFilePathForStreaming(&ctx, fid); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		file, err := storage.GetFile(ctx, path)

		if err != nil {
			http.Error(w, "We could not retrieve your file :(", http.StatusInternalServerError)
			return
		}
		if stream {
			streamVideo(w, file)
			return
		}
		sendFileToWriter(w, file)
	})
}

func streamVideo(w http.ResponseWriter, file *os.File) {
	defer file.Close()

	buf := bytes.NewBuffer(nil)

	_, err := io.Copy(buf, file)
	if err != nil {
		http.Error(w, cannotUse, http.StatusInternalServerError)
		return
	}

	FHeader := make([]byte, 512)
	file.Seek(0, 0)
	file.Read(FHeader)
	// Get content type of file
	FileContentType := http.DetectContentType(FHeader)

	// Get the file size
	FileStat, _ := file.Stat()
	FileSize := strconv.FormatInt(FileStat.Size(), 10)

	w.Header().Set("Accept-Ranges", "bytes")
	w.Header().Set("Content-Type", FileContentType)
	w.Header().Set("Content-Length", FileSize)

	if err = file.Close(); err != nil && err != os.ErrInvalid {
		http.Error(w, "close file error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(206)
	w.Write(buf.Bytes())
}

func sendFileToWriter(w http.ResponseWriter, file *os.File) {
	defer file.Close()

	FHeader := make([]byte, 512)
	file.Read(FHeader)
	// Get content type of file
	FileContentType := http.DetectContentType(FHeader)

	// Get the file size
	FileStat, _ := file.Stat()
	FileSize := strconv.FormatInt(FileStat.Size(), 10)

	nameslice := strings.Split(file.Name(), "/")
	fname := nameslice[len(nameslice)-1]
	// Send the headers
	w.Header().Set("Content-Disposition", "attachment; filename="+fname)
	w.Header().Set("Content-Type", FileContentType)
	w.Header().Set("Content-Length", FileSize)

	// Send the file
	// Reset file's cursor
	file.Seek(0, 0)
	io.Copy(w, file)
	file.Close()
}

// GetSharedFIle is the handler for downloading a public file
func GetSharedFile(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)

		var (
			path string
			err  error
		)

		if path, err = database.GetSharedFilePath(&ctx, vars["link"]); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		file, err := storage.GetFile(ctx, path)

		if err != nil {
			http.Error(w, "We could not retrieve your file :(", http.StatusInternalServerError)
			return
		}

		sendFileToWriter(w, file)
	})
}

// GetFile is the handler for downloading a file
func GetFolder(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)

		fid, err := strconv.Atoi(vars["id"])
		if err != nil {
			http.Error(w, "Wrong file ID format", http.StatusBadRequest)
			return
		}

		var (
			path string
			uid  int
			ok   bool
		)
		if uid, ok = extractUserID(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		if path, err = database.GetFolderPath(&ctx, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = archiver.Zip.Write(w, []string{ctx.StoragePath + path}); err != nil {
			logger.Error().Str("uuid", r.Form.Get("uuid")).Err(err).Msg("GetFolder.archiver")
			http.Error(w, "We could not ZIP your file :(", http.StatusInternalServerError)
			return
		}

		return
	})
}

// DeleteFile is the handler to delete a file
func DeleteFile(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var (
			err  error
			path string
		)
		fid, err := strconv.Atoi(vars["id"])
		if err != nil {
			http.Error(w, "Wrong file ID format", http.StatusBadRequest)
			return
		}

		var (
			uid int
			vol string
			ok  bool
		)
		if uid, vol, ok = extractAllFromToken(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}
		if path, err = database.GetFilePath(&ctx, vol, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = database.FlagDeleteFile(&ctx, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		if err = storage.DeleteFile(&ctx, path); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = database.DeleteFile(&ctx, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.Write([]byte("Success"))
	})
}

func ShareFile(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		fid, err := strconv.Atoi(mux.Vars(r)["id"])
		if err != nil {
			http.Error(w, "Wrong file ID format", http.StatusBadRequest)
			return
		}
		var (
			uid  int
			ok   bool
			link string
		)

		if uid, ok = extractUserID(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		if link, err = database.ShareFile(&ctx, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write([]byte("/direct/" + link))
	})
}

// DeleteFile is the handler to delete a file
func RenameFile(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var (
			input database.File
			err   error
		)

		err = json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			DecodeLogger.Error().Str("uuid", r.Form.Get("uuid")).Interface("input", r.Body).Err(err).Msg("RenameFile")
			http.Error(w, "Could not parse input", http.StatusBadRequest)
			return
		}

		var (
			path string
			uid  int
			ok   bool
			vol  string
		)

		if uid, vol, ok = extractAllFromToken(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		if path, err = database.GetFilePath(&ctx, vol, int(input.ID), uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = database.RenameFile(&ctx, input.Name, int(input.ID), uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		tmp := strings.Split(path, "/")
		newpath := strings.Join(append(tmp[:len(tmp)-1], input.Name), "/")

		if err = storage.RenameFile(&ctx, path, newpath); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = database.UpdateFileLink(&ctx, int(input.ID), uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Write([]byte("Success"))
	})
}

// DeleteFolder is the handler to delete a folder
func DeleteFolder(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var (
			err  error
			path string
		)
		fid, err := strconv.Atoi(vars["id"])
		if err != nil {
			http.Error(w, "Wrong file ID format", http.StatusBadRequest)
			return
		}

		var (
			uid int
			ok  bool
		)

		if uid, ok = extractUserID(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		if err = database.FlagDeleteFolder(&ctx, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = storage.DeleteFolder(&ctx, path); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = database.DeleteFolder(&ctx, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.Write([]byte("Success"))
	})
}

// RenameFolder is the handler to rename a folder
func RenameFolder(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "not yet implemented", http.StatusNotImplemented)
		return
		vars := mux.Vars(r)
		var (
			err  error
			path string
		)
		fid, err := strconv.Atoi(vars["id"])
		if err != nil {
			http.Error(w, "Wrong file ID format", http.StatusBadRequest)
			return
		}

		var (
			uid int
			ok  bool
		)

		if uid, ok = extractUserID(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		var input database.Folder

		err = json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			DecodeLogger.Error().Str("uuid", r.Form.Get("uuid")).Interface("input", r.Body).Err(err).Msg("RenameFolder")
			http.Error(w, "Could not parse input", http.StatusBadRequest)
			return
		}

		if err = database.RenameFolder(&ctx, input.Path, fid, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		if err = storage.DeleteFolder(&ctx, path); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		//if err = database.RenameFolder(&ctx, fid, uid); err != nil {
		//	http.Error(w, err.Error(), http.StatusInternalServerError)
		//}
		w.Write([]byte("Success"))
	})
}

type foldercreation struct {
	Path string `json:"path"`
}

func decodeFolder(rdr io.Reader) (string, bool) {
	var i foldercreation
	err := json.NewDecoder(rdr).Decode(&i)
	if err != nil {
		DecodeLogger.Error().Interface("input", rdr).Err(err).Msg("decodeFolder")
		return "", false
	}

	return i.Path, true
}

// AddFolder is the handler to create a new folder
func AddFolder(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			err  error
			path string
		)

		defer api.LogAction(r.Form.Get("uuid"), &err)

		var (
			uid int
			ok  bool
			vol string
		)

		if path, ok = decodeFolder(r.Body); !ok {
			DecodeLogger.Error().Str("uuid", r.Form.Get("uuid")).Interface("input", r.Body).Err(err).Msg("AddFolder")
			http.Error(w, "Could not decode infos :(", http.StatusBadRequest)
			return
		}

		if uid, vol, ok = extractAllFromToken(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		var folderID int

		if path, folderID, err = database.AddFolder(&ctx, path, uid); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = storage.AddFolder(&ctx, vol, path); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write([]byte(strconv.Itoa(folderID)))
	})
}
