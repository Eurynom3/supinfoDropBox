package handlers

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/Eurynom3/supinfoDropBox/api"
	"gitlab.com/Eurynom3/supinfoDropBox/api/storage"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/Eurynom3/supinfoDropBox/api/database"
)

const connectionFailed = "Failed to connect :("
const alreadyExists = "User already exists"
const insufficentStorage = "There is no space left :("
const creationFailed = "Failed to create your account :("
const wrongPassword = "Incorrect password"
const passwordFail = "Fail to change your password :("

type AuthAnswer struct {
	Token  string `json:"token"`
	UserID uint   `json:"userID"`
	Volume string
}

// Connect is the handler to login
func Connect(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var err error

		defer api.LogAction(r.Form.Get("uuid"), &err)

		input, ok := decodeInput(r.Body)
		if !ok {
			http.Error(w, connectionFailed, http.StatusBadRequest)
			return
		}

		if ans, success := connect(&ctx, input, w, r); success {
			json.NewEncoder(w).Encode(ans)
		}

	})
}

func connect(ctx *api.SUPFileCtx, input input, w http.ResponseWriter, r *http.Request) (*AuthAnswer, bool) {
	logger.Info().Msg("[CONNECT]" + input.Login + " Tries to connect")

	userID, volume, logged, err := database.Auth(ctx, input.Login, input.Password)
	if err != nil {
		http.Error(w, connectionFailed, http.StatusInternalServerError)
		return nil, false
	}

	if !logged {
		http.Error(w, "This account and/or password does not exists", http.StatusUnauthorized)
		return nil, false
	}

	t := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userID": userID,
		"volume": volume,
		"exp":    time.Now().Add(time.Hour * 1).Unix(),
		"nbf":    time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
	})

	tokenString, err := t.SignedString([]byte(viper.GetString("secret")))
	if err != nil {
		http.Error(w, "Error while signing token !", http.StatusInternalServerError)
		logger.Error().Err(err).Msg("Connect")

		return nil, false
	}

	return &AuthAnswer{
		Token:  "Bearer " + tokenString,
		UserID: userID,
		Volume: volume,
	}, true
}

// Register is the handler to create a new user
func Register(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var err error
		defer api.LogAction(r.Form.Get("uuid"), &err)

		input, ok := decodeInput(r.Body)
		if !ok {
			http.Error(w, connectionFailed, http.StatusBadRequest)
			return
		}

		if success := register(&ctx, input, w, r); success {
			w.Write([]byte("OK"))

		}

	})
}

type passChange struct {
	OldPass string `json:"current"`
	NewPass string `json:"pwd"`
}

func ChangePassword(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var err error
		defer api.LogAction(r.Form.Get("uuid"), &err)

		pass, ok := decodePasswords(r.Body)
		if !ok {
			http.Error(w, connectionFailed, http.StatusBadRequest)
			return
		}

		var (
			uid int
		)

		if uid, ok = extractUserID(r); !ok {
			http.Error(w, illegalToken, http.StatusUnauthorized)
			return
		}

		if err = database.ChangePasswords(&ctx, uid, pass.OldPass, pass.NewPass); err != nil {
			if strings.Contains(err.Error(), "wrong password") {
				http.Error(w, wrongPassword, http.StatusUnauthorized)
				return
			}
			http.Error(w, passwordFail, http.StatusInternalServerError)
			return
		}

	})
}

func register(ctx *api.SUPFileCtx, input input, w http.ResponseWriter, r *http.Request) bool {
	var (
		id  int
		vol string
		err error
	)

	if id, vol, err = database.Register(ctx, input.Login, input.Password); err != nil {
		if strings.Contains(err.Error(), "duplicate key") {
			http.Error(w, alreadyExists, http.StatusConflict)
			return false
		} else if strings.Contains(err.Error(), "not enough space") {
			logger.Warn().Msg("Can't create new user, not enough storage !")
			http.Error(w, insufficentStorage, http.StatusInsufficientStorage)
			return false
		}
		http.Error(w, creationFailed, http.StatusInternalServerError)
		return false
	}

	if err = storage.AddFolder(ctx, vol, strconv.Itoa(id)); err != nil {
		http.Error(w, creationFailed, http.StatusInternalServerError)
		return false
	}
	return true
}

func RegisterIfNotLogin(ctx api.SUPFileCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		defer api.LogAction(r.Form.Get("uuid"), &err)

		input, ok := decodeInput(r.Body)
		if !ok {
			http.Error(w, connectionFailed, http.StatusBadRequest)
			return
		}

		// Connect
		if ans, success := connect(&ctx, input, w, r); success {
			json.NewEncoder(w).Encode(ans)

			// If can't connect, register it
		} else {
			if success = register(&ctx, input, w, r); success {

				// It's now registered, connect again
				if ans, success := connect(&ctx, input, w, r); success {
					json.NewEncoder(w).Encode(ans)
				}
			}
			// If we could not register it, it's handled in register func
		}
	})
}

type input struct {
	Login    string `json:"email"`
	Password string `json:"password"`
}

func decodeInput(rdr io.Reader) (input, bool) {
	var i input
	err := json.NewDecoder(rdr).Decode(&i)
	if err != nil {
		DecodeLogger.Error().Interface("input", rdr).Err(err).Msg("decodeInput")
		return input{}, false
	}

	return i, true
}

func decodePasswords(rdr io.Reader) (passChange, bool) {
	var i passChange
	err := json.NewDecoder(rdr).Decode(&i)
	if err != nil {
		DecodeLogger.Error().Interface("input", rdr).Err(err).Msg("decodeInput")
		return passChange{}, false
	}

	return i, true
}

const illegalToken = "Illegal token, missing informations"

func extractUserID(r *http.Request) (int, bool) {
	id, ok := r.Context().Value("token").(*jwt.Token).Claims.(jwt.MapClaims)["userID"].(float64)
	return int(id), ok
}

func extractAllFromToken(r *http.Request) (int, string, bool) {
	id, ok := r.Context().Value("token").(*jwt.Token).Claims.(jwt.MapClaims)["userID"].(float64)
	if !ok {
		return 0, "", ok
	}
	vol, ok := r.Context().Value("token").(*jwt.Token).Claims.(jwt.MapClaims)["volume"].(string)
	return int(id), vol, ok
}
