package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/Eurynom3/supinfoDropBox/api"
	"gitlab.com/Eurynom3/supinfoDropBox/api/middlewares"
)

func Get(ctx api.SUPFileCtx) http.Handler {
	r := mux.NewRouter()

	// Routes
	//r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/health", HealthCheck)
	r.HandleFunc("/debug", Debug)
	r.HandleFunc("/debug/{params:.*}", Debug)

	// Files
	r.Handle("/file", middlewares.AddFileChain().Then(AddFile(ctx))).Methods(http.MethodPost, http.MethodOptions)
	r.Handle("/file/{id:[0-9]+}", middlewares.GenericChain().Then(GetFile(ctx, false))).Methods(http.MethodPost, http.MethodGet, http.MethodOptions)
	r.Handle("/file", middlewares.GenericChain().Then(RenameFile(ctx))).Methods(http.MethodPut, http.MethodOptions)
	r.Handle("/file/share/{id:[0-9]+}", middlewares.GenericChain().Then(ShareFile(ctx))).Methods(http.MethodGet)
	r.Handle("/file/{id:[0-9]+}", middlewares.GenericChain().Then(DeleteFile(ctx))).Methods(http.MethodDelete)
	r.Handle("/direct/{link:.*}", middlewares.CoreChain().Then(GetSharedFile(ctx))).Methods(http.MethodGet)
	r.Handle("/stream/{id:[0-9]+}", middlewares.CoreChain().Then(GetFile(ctx, true))).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)

	//r.HandleFunc("/moveFile", ArticlesHandler)

	// Folders
	r.Handle("/folder", middlewares.GenericChain().Then(AddFolder(ctx))).Methods(http.MethodPost, http.MethodOptions)
	r.Handle("/folder/{id:[0-9]+}", middlewares.GenericChain().Then(DeleteFolder(ctx))).Methods(http.MethodDelete)
	r.Handle("/folder", middlewares.GenericChain().Then(RenameFolder(ctx))).Methods(http.MethodPut, http.MethodOptions)
	r.Handle("/folder/{id:[0-9]+}", middlewares.GenericChain().Then(GetFolder(ctx))).Methods(http.MethodPost, http.MethodOptions)
	r.Handle("/folder/content", middlewares.GenericChain().Then(GetFolderContent(ctx))).Methods(http.MethodGet)

	// Users
	r.Handle("/connect", middlewares.CoreChain().Then(Connect(ctx))).Methods(http.MethodPost, http.MethodOptions)
	r.Handle("/register", middlewares.CoreChain().Then(Register(ctx))).Methods(http.MethodPost, http.MethodOptions)
	r.Handle("/password", middlewares.GenericChain().Then(ChangePassword(ctx))).Methods(http.MethodPost, http.MethodOptions)
	r.Handle("/google/connect", middlewares.CoreChain().Then(RegisterIfNotLogin(ctx))).Methods(http.MethodPost, http.MethodOptions)

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./front/")))

	http.Handle("/", r)

	return r
}

// HealthCheck is a handler to test if the api is still able to answer requests
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Healthy"))
}

type debuggingVars struct {
	PathParams map[string]string   `json:"pathParams"`
	GetParams  map[string][]string `json:"getParams"`
}

func Debug(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	d := debuggingVars{
		PathParams: mux.Vars(r),
		GetParams:  r.Form,
	}
	json.NewEncoder(w).Encode(d)
}
