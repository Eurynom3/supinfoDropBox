package api

import (
	"runtime"

	"github.com/rs/zerolog/log"
)

const stacktraceError = "Failed to fetch stacktrace"

func LogAction(uuid string, err *error) {
	_, f, _, ok := runtime.Caller(1)
	if !ok {
		log.Error().Msg(stacktraceError)
	}
	if err != nil {
		log.Error().Str("uuid", uuid).Msgf("Executed function %s : ERROR %v", f, *err)
		return
	}
	log.Error().Str("uuid", uuid).Msgf("Executed function %s", f)
}
