package middlewares

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"
)

func Recover(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		defer func() {

			if r := recover(); r != nil {
				http.Error(w, "fail", http.StatusInternalServerError)
				switch err := r.(type) {
				case string:
					log.Error().Str("stacktrace", identifyPanic()).Msg("PANIC :" + err)

				case error:
					log.Error().Str("stacktrace", identifyPanic()).Msg("panic :" + err.Error())

				default:
					log.Error().Str("stacktrace", identifyPanic()).Interface("err", err).Msg("panic, unknown type")

				}

			}
		}()

		next.ServeHTTP(w, r)

	})

}

func ParseForm(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		next.ServeHTTP(w, r)
	})
}

func Timer(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		startTime := time.Now()

		next.ServeHTTP(w, r)

		log.Info().Msg(strconv.Itoa(int(time.Since(startTime).Seconds())))
	})

}

func NewRequests(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		u, err := uuid.NewV4()
		if err != nil {
			panic(err)
		}
		r.Form.Add("uuid", u.String())

		next.ServeHTTP(w, r)

	})

}

func CheckParameters(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}

func identifyPanic() string {
	var name, file string
	var line int
	var pc [16]uintptr

	n := runtime.Callers(3, pc[:])
	for _, pc := range pc[:n] {
		fn := runtime.FuncForPC(pc)
		if fn == nil {
			continue
		}
		file, line = fn.FileLine(pc)
		name = fn.Name()
		if !strings.HasPrefix(name, "runtime.") {
			break
		}
	}

	switch {
	case name != "":
		return fmt.Sprintf("%v:%v", name, line)
	case file != "":
		return fmt.Sprintf("%v:%v", file, line)
	}

	return fmt.Sprintf("pc:%x", pc)
}
