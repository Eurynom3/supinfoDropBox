package middlewares

import (
	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/justinas/alice"
	"github.com/spf13/viper"
)

// GenericChain returns a chain useful for any request
func GenericChain() alice.Chain {

	cc := CoreChain()
	return cc.Append(

		jwtmiddleware.New(jwtmiddleware.Options{
			ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
				return []byte(viper.GetString("secret")), nil
			},
			//Debug: true,
			UserProperty:  "token",
			SigningMethod: jwt.SigningMethodHS256,
		}).Handler,

	)
}

func CoreChain() alice.Chain {
	return alice.New(
		Recover,
		//Timer,
		ParseForm,
		NewRequests,
		CheckParameters,
	)
}

// AddFileChain append to a generic chain middlewares to prepare file uploading
func AddFileChain() alice.Chain {
	gc := GenericChain()
	return gc.Append(
		IsAMultipart,
	)
}
