package middlewares

import (
	"net/http"

	"gitlab.com/Eurynom3/supinfoDropBox/api"
	"gitlab.com/Eurynom3/supinfoDropBox/api/database"
	"gitlab.com/Eurynom3/supinfoDropBox/api/storage"
)

func IsAMultipart(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if err := r.ParseMultipartForm(10 * storage.MB); err != nil {
			http.Error(w, "Not a multipart request", http.StatusBadRequest)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func FilePresent(ctx *api.SUPFileCtx) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			userID, fileName := r.Form.Get("userID"), r.Form.Get("file")

			filePresent, err := database.IsFilePresent(ctx, userID, fileName)
			if err != nil {
				panic(err)
			}

			if filePresent {
				http.Error(w, "File already exists", 400)
			}

		})
	}
}
