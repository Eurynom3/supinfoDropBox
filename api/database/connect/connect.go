package connect

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

)

func Database(root string) *sql.DB {

	db, err := sql.Open(
		"postgres",

		fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			viper.GetString(root + ".host"),
			viper.GetInt(root + ".port"),
			viper.GetString(root + ".user"),
			viper.GetString(root + ".password"),
			viper.GetString(root + ".name")),
	)

	if err != nil {
		panic(err)
	}

	db.SetMaxOpenConns(50)
	db.SetMaxIdleConns(50)
	db.SetConnMaxLifetime(50*time.Second)

	log.Info().Msg("Connected to DB")

	return db

}