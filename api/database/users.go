package database

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/lib/pq"
	"gitlab.com/Eurynom3/supinfoDropBox/api"
	"gitlab.com/Eurynom3/supinfoDropBox/api/storage"
)

func Auth(ctx *api.SUPFileCtx, user, password string) (uint, string, bool, error) {

	var (
		correct bool
		userID  uint
		volume string
		err     error
	)

	query := "SELECT users.id, (SELECT disks.path FROM disks WHERE disks.id = users.disk_id LIMIT 1), users.password = crypt($2, password) FROM users WHERE login = $1;"
	args := []interface{}{user, password}

	err = ctx.DB.QueryRow(query, args...).Scan(&userID,&volume, &correct )
	if err != nil {
		SQLlogger.Error().Err(err).Msg("Auth")
		return 0, "", false, err
	}

	if !correct {
		return 0, "", false, errors.New(fmt.Sprintf("User %s has failed to connect or does not exist", user))
	}
	return userID, volume, true, nil

}

func ChangePasswords(ctx *api.SUPFileCtx, userID int, oldPass, newPass string) (error) {

	var (
		found  int
		err     error
	)

	query := "UPDATE users SET password = crypt($1, gen_salt('md5')) WHERE password = crypt($2, password) AND id = $3 RETURNING id;"
	args := []interface{}{newPass, oldPass, userID}

	err = ctx.DB.QueryRow(query, args...).Scan(&found)
	if err != nil {
		if err == sql.ErrNoRows {
			return errors.New("wrong password")
		}
		SQLlogger.Error().Err(err).Msg("ChangePasswords")
		return err
	}

	return nil

}


func Register(ctx *api.SUPFileCtx, user, password string) (int, string, error) {

	if enough, err := EnoughSpace(ctx, 30*storage.GB); err != nil {
		return 0, "",err
	} else if !enough {
		return 0, "",errors.New("not enough space")
	}
	
	// Adding user
	query := `INSERT INTO users (login, password, allocated_bytes, disk_id) 
		VALUES 
		(
			$1, 
			crypt($2, gen_salt('md5')), 
			$3, 
			(
				SELECT id 
				FROM disks as d
				ORDER BY 
				(
					size - 
					(
						SELECT coalesce(sum(allocated_bytes), 0) 
						FROM users 
						WHERE disk_id = d.id
					)
				) DESC
				LIMIT 1
			)
		) RETURNING id, (SELECT d.path FROM disks AS d WHERE d.id = disk_id);`
	args := []interface{}{user, password, 30 * storage.GB}

	var (
		id int
		vol string
	)
	err := ctx.DB.QueryRow(query, args...).Scan(&id, &vol)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("Register.User")
		return 0, "",err
	}

	query = "INSERT INTO folders (path, user_access, user_id) VALUES ('root', $1::bigint[], $2);"
	args = []interface{}{pq.Array([]int{id}), id}

	_, err = ctx.DB.Exec(query, args...)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("Register.Folder")
		query = "DELETE FROM users WHERE id = $1;"
		_, _ = ctx.DB.Exec(query, id)

		return 0, "",err
	}
	return id, vol, nil

}
