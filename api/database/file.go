package database

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/lib/pq"
	"github.com/rs/zerolog"
	"github.com/satori/go.uuid"
	"gitlab.com/Eurynom3/supinfoDropBox/api"
)

var (
	SQLlogger  zerolog.Logger
	ScanLogger zerolog.Logger
)

func init() {
	SQLlogger = zerolog.New(os.Stderr).With().Timestamp().Str("type", "[SQL]").Logger()
	ScanLogger = zerolog.New(os.Stderr).With().Timestamp().Str("type", "[SCAN]").Logger()

}

func IsFilePresent(ctx *api.SUPFileCtx, user string, fileName string) (bool, error) {

	var (
		res int
		err error
	)

	query := "SELECT count(*) FROM files WHERE user = $1 and name = $2;"
	args := []interface{}{user, fileName}

	err = ctx.DB.QueryRow(query, args...).Scan(&res)
	if err != nil {
		return false, err
	}

	if res != 1 {
		if res == 0 {
			return false, nil
		} else {
			return false, errors.New(fmt.Sprintf("User %s has two files with name %s", user, fileName))
		}
	} else {
		return true, nil
	}
}

type File struct {
	ID               uint      `json:"id"`
	Name             string    `json:"name"`
	Size             uint      `json:"size"`
	Extension        string    `json:"extension"`
	CreationDate     time.Time `json:"creationDate"`
	LastModification time.Time `json:"lastModification"`
}

func GetFiles(ctx *api.SUPFileCtx, path string, userID int) ([]File, error) {

	path = front2backPath(path)

	query := "SELECT id, name, size, extension, creation_date, last_modification FROM files WHERE folder_id = (SELECT id FROM folders WHERE path <@ $2 AND user_access <@ $1::bigint[] ORDER BY path LIMIT 1);"
	args := []interface{}{pq.Array([]int{userID}), path}

	rows, err := ctx.DB.Query(query, args...)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("GetFiles")
		return nil, err
	}
	defer rows.Close()

	var files []File
	var tmpfile File
	for rows.Next() {
		if err = rows.Scan(&tmpfile.ID, &tmpfile.Name, &tmpfile.Size, &tmpfile.Extension, &tmpfile.CreationDate, &tmpfile.LastModification); err != nil {
			ScanLogger.Error().Err(err).Msg("GetFiles")
			return nil, err
		}
		files = append(files, tmpfile)
	}
	return files, nil
}

type Folder struct {
	ID   uint   `json:"id"`
	Path string `json:"path"`
}

func EnoughSpace(ctx *api.SUPFileCtx, needed int64) (bool, error) {
	query := "SELECT count(id) > 0 FROM disks WHERE (size - (SELECT coalesce(sum(allocated_bytes), 0) FROM users WHERE disk_id = disks.id)) > $1;"
	args := []interface{}{needed}

	var enough bool
	err := ctx.DB.QueryRow(query, args...).Scan(&enough)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("EnoughSpace")
		return false, err
	}
	return enough, nil
}

func EnoughSpaceForUser(ctx *api.SUPFileCtx, needed int64, userid int) (bool, error) {
	query := "SELECT (allocated_bytes - (SELECT coalesce(sum(size), 0) FROM files WHERE user_id = users.id)) > $1 FROM users WHERE id = $2;"
	args := []interface{}{needed, userid}

	var enough bool
	err := ctx.DB.QueryRow(query, args...).Scan(&enough)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("EnoughSpaceForUser")
		return false, err
	}
	return enough, nil
}

func GetFolders(ctx *api.SUPFileCtx, path string, userID int) (int, []Folder, error) {

	path = front2backPath(path)
	query := "SELECT id, path FROM folders WHERE path <@ $2  AND user_access <@ $1::bigint[] AND (nlevel(path) - nlevel($2)) < 2 ORDER BY path ;"
	args := []interface{}{pq.Array([]int{userID}), path}

	rows, err := ctx.DB.Query(query, args...)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("GetFolders")
		return 0, nil, err
	}
	defer rows.Close()

	var folders []Folder
	var tmpfolder Folder
	for rows.Next() {
		if err = rows.Scan(&tmpfolder.ID, &tmpfolder.Path); err != nil {
			ScanLogger.Error().Err(err).Msg("GetFolders")
			return 0, nil, err
		}
		tmpfolder.Path = back2frontPath(tmpfolder.Path)
		folders = append(folders, tmpfolder)
	}
	if len(folders) == 0 {
		err = errors.New("current path not found")
		SQLlogger.Error().Err(err).Msg("GetFolders")

		return 0, nil, err
	}

	currentFolderID := folders[0].ID
	folders = folders[1:]
	return int(currentFolderID), folders, nil
}

func back2frontPath(path string) string {
	return strings.Replace(strings.Replace(path, "root", "", 1), ".", "/", -1) + "/"
}

func front2backPath(path string) string {
	if path == "/" {
		return "root"
	}
	if path[len(path)-1] == '/' {
		path = path[:len(path)-1]
	}
	return strings.Replace("root"+path, "/", ".", -1)
}

func AddFile(ctx *api.SUPFileCtx, path, volume, fileName, extension string, size int64, userID int) (string, int, error) {

	if enough, err := EnoughSpaceForUser(ctx, size, userID); err != nil {
		return "", 0, err
	} else if !enough {
		return "", 0, errors.New("not enough space")
	}

	path = front2backPath(path)
	query := "INSERT INTO files (name, storage_name, extension, size, user_id, folder_id) VALUES ($1, $1, $2, $3, $4, (SELECT id FROM folders WHERE path = $5 AND user_access <@ $6::bigint[] LIMIT 1)) RETURNING id;"
	args := []interface{}{fileName, extension, size, userID, path, pq.Array([]int{userID})}

	var insertID int
	err := ctx.DB.QueryRow(query, args...).Scan(&insertID)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("AddFile")
		return "", 0, err
	}
	return logPath2PhyPath(path, volume, fileName, strconv.Itoa(userID)), insertID, err
}

const (
	statusDelete = "todelete"
	statusRename = "torename"
	statusSynced = "synced"
)

func FlagDeleteFile(ctx *api.SUPFileCtx, fileID, userID int) (error) {
	query := "UPDATE files SET current_status = $1 WHERE id = $2 AND user_id = $3;"
	args := []interface{}{statusDelete, fileID, userID}
	return applyModification(ctx, "ChangeStatus", query, args)
}

func RenameFile(ctx *api.SUPFileCtx, name string, fileID, userID int) (error) {
	query := "UPDATE files SET current_status = $1, name = $2 WHERE id = $3 AND user_id = $4;"
	args := []interface{}{statusRename, name, fileID, userID}
	return applyModification(ctx, "ChangeStatus", query, args)
}

func FlagDeleteFolder(ctx *api.SUPFileCtx, folderID, userID int) (error) {
	query := "UPDATE folders SET current_status = $1 WHERE id = $2 AND user_id = $3;"
	args := []interface{}{statusDelete, folderID, userID}
	return applyModification(ctx, "ChangeStatus", query, args)
}

func RenameFolder(ctx *api.SUPFileCtx, name string, folderID, userID int) (error) {
	query := "UPDATE folders SET current_status = $1, name = $2 WHERE id = $3 AND user_id = $4;"
	args := []interface{}{statusRename, name, folderID, userID}
	return applyModification(ctx, "ChangeStatus", query, args)
}

func DeleteFile(ctx *api.SUPFileCtx, fileID, userID int) (error) {

	query := "DELETE FROM files WHERE id = $1 and user_id = $2;"
	args := []interface{}{fileID, userID}

	return applyModification(ctx, "DeleteFile", query, args)
}

func applyModification(ctx *api.SUPFileCtx, operation, query string, args []interface{}) (err error) {
	_, err = ctx.DB.Exec(query, args...)
	if err != nil {
		SQLlogger.Error().Err(err).Msg(operation)
	}
	return
}

func UpdateFileLink(ctx *api.SUPFileCtx, fileID, userID int) (error) {
	query := "UPDATE files SET storage_name = name, current_status = $1 WHERE id = $2 and user_id = $3;"
	args := []interface{}{statusSynced, fileID, userID}
	return applyModification(ctx, "RenameFile", query, args)
}

func UpdateFolderLink(ctx *api.SUPFileCtx, folderID, userID int) (error) {
	query := "UPDATE folders SET storage_name = name, current_status = $1 WHERE id = $2 and user_id = $3;"
	args := []interface{}{statusSynced, folderID, userID}
	return applyModification(ctx, "RenameFile", query, args)
}

func AddFolder(ctx *api.SUPFileCtx, path string, userID int) (string, int, error) {

	path = front2backPath(path)
	query := "INSERT INTO folders(path, user_access, previous_folder, user_id) SELECT $1::ltree as path, user_access, id, $2 as user_id FROM folders WHERE user_access <@ $3::bigint[] AND path @> $1 ORDER BY path DESC LIMIT 1 RETURNING id;"
	args := []interface{}{path, userID, pq.Array([]int{userID})}

	var id int

	err := ctx.DB.QueryRow(query, args...).Scan(&id)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("AddFolder")
	}
	return logFolder2PhyFolder(path, strconv.Itoa(userID)), id, err
}

func DeleteFolder(ctx *api.SUPFileCtx, fileID, userID int) (err error) {

	// Cascade on delete
	query := "DELETE FROM folders WHERE id = $1 and user_id = $2;"
	args := []interface{}{fileID, userID}

	return applyModification(ctx, "DeleteFolder", query, args)
}

func GetFilePath(ctx *api.SUPFileCtx, volume string, fileID, userID int) (string, error) {

	var folders, file, ownerID string
	query := "SELECT folders.path, files.storage_name, files.user_id FROM files INNER JOIN folders ON files.folder_id = folders.id WHERE files.id = $1 AND folders.user_access <@ $2::bigint[];"
	args := []interface{}{fileID, pq.Array([]int{userID})}

	err := ctx.DB.QueryRow(query, args...).Scan(&folders, &file, &ownerID)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("GetFilePath")
		return "", err
	}
	return logPath2PhyPath(folders, volume, file, ownerID), err
}

func GetFilePathForStreaming(ctx *api.SUPFileCtx, fileID int) (string, error) {

	var folders, file, volume, ownerID string
	query := "SELECT folders.path, files.storage_name, (SELECT disks.path FROM disks JOIN users ON disks.id = users.disk_id WHERE users.id = files.user_id LIMIT 1), files.user_id FROM files INNER JOIN folders ON files.folder_id = folders.id WHERE files.id = $1;"
	args := []interface{}{fileID}

	err := ctx.DB.QueryRow(query, args...).Scan(&folders, &file, &volume, &ownerID)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("GetFilePath")
		return "", err
	}
	return logPath2PhyPath(folders, volume, file, ownerID), err
}

func ShareFile(ctx *api.SUPFileCtx, fileID, userID int) (string, error) {
	query := "UPDATE files SET shared = true WHERE id = $1 AND user_id = $2;"
	args := []interface{}{fileID, userID}

	var err error
	if err = applyModification(ctx, "ShareFile", query, args); err != nil {
		return "", err
	}

	link, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	query = "INSERT INTO direct_links(link, file_id) VALUES ($1, $2);"
	args = []interface{}{link.String(), fileID}
	return link.String(), applyModification(ctx, "CreateDirectLink", query, args)
}

func GetSharedFilePath(ctx *api.SUPFileCtx, link string) (string, error) {

	var folders, vol, file, ownerID string
	query := "SELECT folders.path, files.storage_name, (SELECT disks.path FROM disks JOIN users ON disks.id = users.disk_id WHERE users.id = files.user_id LIMIT 1), files.user_id FROM files INNER JOIN folders ON files.folder_id = folders.id WHERE files.id = (SELECT file_id FROM direct_links WHERE link = $1);"
	args := []interface{}{link}

	err := ctx.DB.QueryRow(query, args...).Scan(&folders, &file, &vol, &ownerID)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("GetSharedFilePath")
		return "", err
	}
	return logPath2PhyPath(folders, vol, file, ownerID), err
}

func GetFolderPath(ctx *api.SUPFileCtx, folderID, userID int) (string, error) {

	var folders string
	query := "SELECT path FROM folders WHERE id = $1 AND user_access <@ $2::bigint[];"
	args := []interface{}{folderID, pq.Array([]int{userID})}

	err := ctx.DB.QueryRow(query, args...).Scan(&folders)
	if err != nil {
		SQLlogger.Error().Err(err).Msg("GetFolderPath")
		return "", err
	}
	return logFolder2PhyFolder(folders, strconv.Itoa(userID)), err
}

func logPath2PhyPath(folders, volume, file, userID string) string {
	return volume + "\\" + logFolder2PhyFolder(folders, userID) + "\\" + file
}

func logFolder2PhyFolder(folders, userID string) string {
	return strings.Replace(strings.Replace(folders, "root", userID, 1), ".", "\\", -1)
}
