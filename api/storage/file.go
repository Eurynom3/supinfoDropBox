package storage

import (
	"errors"
	"os"

	"github.com/rs/zerolog"
	"gitlab.com/Eurynom3/supinfoDropBox/api"
)

const (
	KB int64 = 1000
	MB       = KB * 1000
	GB       = MB * 1000
)

var (
	UnknownError = errors.New("something went wrong")
	WriteError   = errors.New("could not write your file :(")
	TooLarge     = errors.New("size too large")
)

var logger zerolog.Logger

func init() {
	logger = zerolog.New(os.Stderr).With().Timestamp().Str("type", "[STORAGE]").Logger()
}

var (
	wait      chan struct{}
	d         = make(chan struct{})
	done      = func() { d <- struct{}{} }
	maxConcur = 10
)

func init() {
	wait = make(chan struct{}, maxConcur)
	for i := 0; i < maxConcur; i++ {
		wait <- struct{}{}
	}
	go func() {
		for {
			<-d
			wait <- struct{}{}
		}
	}()
}

// AddFile write a given multipart request's file to storage server
func AddFile(ctx api.SUPFileCtx, path string, fileBytes []byte) (error) {

	// Rate limiting
	<-wait
	defer done()

	newFile, err := os.Create(ctx.StoragePath + path)
	defer newFile.Close()
	if err != nil {
		logger.Error().Err(err).Msg("AddFile.Create")
		return WriteError
	}

	// 32K buffer copy
	//var written int64
	if _, err := newFile.Write(fileBytes); err != nil {
		logger.Error().Err(err).Msg("AddFile.Write")
		return WriteError
	}

	return nil
}

var illegalFormats = []string{
	"application/octet-stream",
	"application/x-msdownload",
	"application/exe",
	"application/x-exe",
	"application/dos-exe",
	"vms/exe",
	"application/x-winexe",
	"application/msdos-windows",
	"application/x-msdos-program",
}

func HasAnIllegalFormats(ext string) bool {
	for _, ilfo := range illegalFormats {
		if ext == ilfo {
			return true
		}
	}
	return false
}

// GetFile returns a file copied from server
func GetFile(ctx api.SUPFileCtx, path string) (file *os.File, err error) {
	<-wait
	defer done()
	file, err = os.Open(ctx.StoragePath + path)
	if err != nil {
		logger.Error().Err(err).Msg("GetFile")
	}

	return
}

// DeleteFile deletes a file
func DeleteFile(ctx *api.SUPFileCtx, path string) (error) {
	<-wait
	defer done()
	err := os.Remove(ctx.StoragePath + path)
	if err != nil {
		logger.Error().Err(err).Msg("DeleteFile")
		return UnknownError
	}
	return nil
}

// DeleteFolder deletes a folder
func DeleteFolder(ctx *api.SUPFileCtx, path string) (error) {
	<-wait
	defer done()
	err := os.RemoveAll(ctx.StoragePath + path)
	if err != nil {
		logger.Error().Err(err).Msg("DeleteFolder")
		return UnknownError
	}
	return nil
}

// AddFolder creates a folder
func AddFolder(ctx *api.SUPFileCtx, volume, path string) (error) {
	<-wait
	defer done()
	err := os.Mkdir(ctx.StoragePath+volume+"\\"+path, os.ModePerm)
	if err != nil {
		logger.Error().Err(err).Msg("AddFolder")
		return UnknownError
	}
	return nil
}

// AddFolder creates a folder
func RenameFile(ctx *api.SUPFileCtx, path, newpath string) (error) {
	<-wait
	defer done()
	err := os.Rename(ctx.StoragePath+path, ctx.StoragePath+newpath)
	if err != nil {
		logger.Error().Err(err).Msg("RenameFile")
		return UnknownError
	}
	return nil
}
